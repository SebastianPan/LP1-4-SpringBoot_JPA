package com.lagou.repository;

import com.lagou.pojo.Artcle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtcleRepository extends JpaRepository<Artcle, Integer> {
}
