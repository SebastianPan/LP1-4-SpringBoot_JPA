package com.lagou.service;

import com.lagou.pojo.Artcle;
import com.lagou.repository.ArtcleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtcleService {

    @Autowired
    private ArtcleRepository artcleRepository;

    public Page<Artcle> findAll(Pageable pageable){
        return artcleRepository.findAll(pageable);
    };



}
