package com.lagou.controller;


import com.lagou.pojo.Artcle;
import com.lagou.service.ArtcleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("api/artcle")
public class ArtcleController {

    @Autowired
    private ArtcleService artcleService;

    @RequestMapping("all")
    public String findAll(Integer pageNum, Model model){
        if(ObjectUtils.isEmpty(pageNum) || pageNum == 0){
            pageNum = 1;
        }
        Page<Artcle> data = artcleService.findAll(PageRequest.of(pageNum-1, 2, Sort.by(Sort.Direction.DESC, "modified")));
        model.addAttribute("data",data);
        return "client/index";
    }


}
